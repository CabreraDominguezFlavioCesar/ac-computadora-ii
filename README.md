# ARQUITECTURA DE COMPUTADORAS - SCD1003 - ISB

## Computadoras Electrónicas

```plantuml
@startmindmap
* Computadoras Electronicas    


** Antecedentes: Computadoras Electromecánicas
*** A inicios del siglo 20 las computadoras de propósito especifico funcionaban para una sola cosa\nY las usaban en el gobierno y los negocios
*** El acelerado crecimiento en la población hizo que la cantidad de datos aumentara\nasí como la necesidad de automatizacion y computacion
*** Al principio las computadoras eran del tamaño de un gabinete,\nempezaron a crecer hasta ocupar toda una habitación,\ncon mantenimiento costoso y ahí nació la necesidad de innovación
*** Una de las computadoras electromecánicas más grandes construidas es la <b>Harvard Mark 1</b>

** Harvard Mark I
*** Construida por IBM en 1944 durante la segunda guerra mundial
**** Tenía 765 mil componentes,\n80 kilómetros de cable,\nun eje de 15 metros que atravesaba toda la estructura\npara mantener en sincronía el dispositivo
**** Utilizaba un motor de 5 caballos de fuerza
*** uno de los usos que se le dio a esta máquina fue hacer simulaciones y cálculos
**** podría ser tres sumas o restas por segundo\nla multiplicación llevaba seis segundos\nla división 15 segundos\nlas operaciones complejas o las funciones trigonométricas llevan más de un minuto
*** la Harvard Mark 1 tenía aproximadamente 3500 relés\nesto provocaba que por pura estadística fallas en un relé al día
*** Otro problema que tenia era que atraian bichos por ser maquinas grandes y calientes
*** En  septiembre de 1947 sacaron una polilla de los relés de la computadora,\ndesde entonces cuando algo sale mal, decimos que tiene bugs (bichos)

** Válvula termo iónica
*** Está formada por un filamento y dos electrodos que están situados dentro de un bulbo de cristal sellado\ncuando el filamento se enciende y se calienta permite el flujo de electrones desde el ánodo hacia el cátodo
*** fueron la base para la radio los teléfonos de larga distancia\ny muchos otros dispositivos electrónicos por cerca de 50 años
*** eran frágiles y se podían quemar como los focos o bulbos de luz
*** 1940 se volvieron más accesibles e hicieron factibles computadoras en base a este componente
*** Cambiar relés por tubos de vacío dio paso a las computadoras electrónicas

** ENIAC
*** Significa calculadora integradora numérica electrónica
*** Construida en 1946 en la universidad de Pensilvania diseñada por John Mauchly y J. Presper Eckerty
*** Fue primera computadora electrónica programable de propósito general 
*** podría realizar 5000 sumas y restas de 10 dígitos por segundo y operó durante 10 años
*** debido a las constantes fallas de los tubos de vacío solo funcionaba durante la mitad del día antes de dañarse

** Transistor
*** en 1947 los científicos los laboratorios bell crearon el transistor
*** el primer transistor en los laboratorios bells se podría cambiar de estados unas 10.000 veces por segundo 
*** los transistores utilizados en las computadoras de hoy  tienen un tamaño menor a 50 nanómetros 
*** Como referencia el ancho de una hoja de papel es de 100.000 nanómetros\ny no solo son diminutos también son muy rápidos\npueden cambiar de estados mi


** IBM 608
*** lanzada en 1957 fue la primera computadora disponible comercialmente basada enteramente en transistores
*** podría realizar cerca de 4500 sumas 80 divisiones o multiplicaciones por segundo 
*** IBM empezó a usar transistores en todos sus productos acercando de esta forma las computadoras a las oficinas en los hogares hoy en día 


@endmindmap
```

## Arquitectura Von Neumann vs Arquitectura Harvard

```plantuml
@startmindmap

* Arquitectura Von Neumann 

**  Ley de Moore
*** Establece que la velocidad del procesador o\npoder de procesamiento total de las computadoras se duplica cada año

*** Electrónica
**** El número de transistores de duplica cada año
**** El costo del chip permanece sin cambios
**** Cada 18 meses se duplica la potencia del cálculo sin modificar el costo

*** Performance
**** Se incrementa la velocidad del procesador
**** Se incrementa la capacidad de la memoria
**** La velocidad de la memoria corre siempre por detrás de la velocidad del procesador

** Funcionalidad de las computadoras 

*** Antes: sistemas cableados
**** Programación mediante hardware
**** Cuando se quiere realizar otra tarea se debe cambiar el hardware

*** Ahora
**** Programación mediante software en cada paso se realiza alguna operación sobre los datos

** Arquitectura Von Neumann

*** Modelo
**** Arquitectura descrita en 1945 por John Von Neumann
**** Los datos y programas se almacenan en una misma memoria de lectura escritura
**** Los contenidos de esta memoria se acceden indicando su posición sin  importar su tipo
**** ejecución secuencial
**** Representación binaria

*** Contiene
**** Tiene 3 componentes principales: la CPU módulo de E/S y la memoria
***** la CPU tiene una unidad de aritmética/lógica tiene una unidad de control y registros
***** Memoria principal la cual puede almacenar tanto instrucciones como datos

***** Sistema de entrada salida: teclado, monitor
**** Todos los componentes se comunican a través de un sistema de buses
***** Bus de datos
***** Bus de direcciones
***** Bus de control

*** Instrucciones
**** La función de la computadora es la ejecución de programas.\n Los programas se encuentran en memoria y consisten en instrucciones
**** La CPU es quien se encarga de ejecutar dichas instrucciones\na través de un ciclo de instrucción
***** La CPU busca la próxima instrucción en memoria
***** Se incrementa el contador el programa
***** La instrucción es decodificada
***** Obtiene de memoria los datos requeridos
***** La unidad de aritmética/lógica ejecuta la instrucción y regresa los resultados en memoria

**** Se implementan lenguajes como el ensamblador
**** Las instrucciones son ejecutadas a grandes velocidades
***** 3.000.000.000 de operaciones por segundo equivalen a 3Ghz

**** Surge el concepto de programa almacenado
**** Separar la memoria y el CPU acarreo un problema\nllamado cuello de botella de Neumann
***** Esto se debe a la diferencia de velocidades del CPU y la memoria
**** Los principios de la arquitectura de Von Neumann se siguen usando hoy en día

** Arquitectura Harvard
*** Modelo
**** Se refiere a las arquitecturas de computadoras que utilizaban dispositivos\Nde almacenamiento físicamente separados para las instrucciones y los datos

*** Contiene
**** Describe una arquitectura de diseño para una computadora\n digital electrónica que consta de varias partes
***** Unidad de procesamiento la cual contiene
****** Unidad de control
******* Lee las instrucciones de la memoria de instrucciones
******* Genera señales de control para obtener los datos de la memoria de datos
****** Unidad aritmética/lógica
******* Ejecuta la instrucción
******* Almacena el resultado en la memoria de datos
****** Registros

***** Memoria de instrucciones
****** Aquí se almacenan las instrucciones de programa a ejecutar
****** La memoria de instrucciones se implementa utilizando memorias no volátiles

***** Memoria de datos
****** Se almacenan los datos utilizados del programa a ejecutar
****** Los datos varían y lo tanto se utiliza la memoria RAM
****** En la RAM se pueden hacer operaciones de lectura/escritura
****** Si es necesario guardar algún dato de manera permanente se utiliza memoria EEPROM o flash

***** Sistema de entrada/salida

*** Memorias
**** Fabricar memorias rápidas tiene un precio muy alto
**** La solución es proporcionar una pequeña cantidad de memoria muy rápida conocida como memoria cache
**** Solucion Harvard
***** Las instrucciones y los datos se almacenan en caches separadas para mejorar el rendimiento
***** Tiene que dividir el cache entre las dos memorias
***** La arquitectura suele utilizarse en PICs o Microcontroladores en productos de propósito especifico



@endmindmap
```
## Basura Electrónica

```plantuml
@startmindmap

* Basura Electronica

** RAEE
*** Residuos de aparatos eléctricos o electrónicos
*** Los aparatos electrónicos cuando dejan de funcionar o tener vida útil se convierten en RAEE
*** Ejemplos de basura electrónica son:
**** celulares
**** computadoras
**** televisores
*** Los residuos crecieron tanto hasta ser un problema ambiental

** DATOS
*** El 70% de las toxinas de los basureros viene de los desechos electrónicos
*** En 2016 la unión europea creo una ley que requiere que cada país\nrequiera 45 toneladas de basura electrónica por cada 100 que se lancen al mercado
*** En Perú hay 400 000 toneladas de basura electrónica
*** En 2012 En Perú el reglamento nacional del manejo de los RAEE\npara su adecuada gestión para fabricantes comercializadoras ,estado y fabricantes
*** En México anualmente se desechan 150 y 180 mil toneladas de basura electrónica
*** Actualmente los aparatos eléctricos tienen una vida útil de 3 a 5 años
*** de los aparatos que se reciclan es el 80% Del sector industrial

** DAÑOS AL AMBIENTE
*** Los residuos contaminan el agua y el aire 
*** los trabajadores de vertederos terminan con problemas de salud
*** Una pila de botón puede contaminar 600 litros de agua

** BENEFICIOS DEL RECICLAJE
*** Se reducen costos de fabricación de nuevos electrónicos
*** Reducción de emisión de gases al ambiente en su creación
*** Brinda beneficios económicos a los individuos o empresas por el reciclaje
*** Accesible a través de los puntos de reciclaje en México
*** Varios minerales se pueden extraer de los circuitos como:
**** Oro
**** Plata
**** Cobre
**** Cobalto
**** Estaño
**** Silicio
**** Hierro
**** Aluminio
*** Y una variedad de plásticos
*** Suelen transformarce en productos reciclados como 
**** Suelas de zapatos
**** Productos automotrices
**** Adornos
**** Productos ecologicos como adoquines
*** Lotes de basura electrónica sacado por empresas suelen funcionar, por ejemplo
**** Computadoras
**** Memorias
**** Ratones
**** Audifonos
**** Cargadores

** Exportación Ilegal
*** Reciclar la basura electrónica de manera segura es un proceso costoso y complicado
*** La mayor parte de la basura electrónica es exportada ilegalmente a países de 3r mundo
**** China
**** Mexico
**** India
*** Derriten los electrónicos para extraer el oro mientras inhalan vapores tóxicos y los residuos terminan en el rio contaminándolo

@endmindmap
```